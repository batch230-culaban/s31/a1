// QUIZ

// 1. What directive is used by Node.js in loading the modules it needs?

 let answ1 = "Require";

// 2. What Node.js module contains a method for server creation?

 let answ2 = "HTTP Module";

// 3. What is the method of the http object responsible for creating a server using Node.js?

 let answ3 = "createServer()";

// 4. What method of the response object allows us to set status codes and content types?
	
 let answ4 = "writeHead()";

// 5. Where will console.log() output its contents when run in Node.js?

 let answ5 = "terminal";

// 6. What property of the request object contains the address's endpoint?

 let answ6 = "url";
